package com.ovys.spatech.spatechapi;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import com.ovys.spatech.spatechapi.SpatechApiApplicationTests;


@RunWith(SpringRunner.class)
@SpringBootTest(classes = SpatechApiApplicationTests.class)
public class SpatechApiApplicationTests {

	@Test
	public void contextLoads() {
	}

}

