/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ovys.spatech.spatechapi.Repositories;


import com.ovys.spatech.spatechapi.Models.Citas;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;

/**
 *
 * @author Cris Murillo
 */
public interface CitasRepository extends MongoRepository<Citas, String>{
    Citas findBy_id(ObjectId _id);
}
