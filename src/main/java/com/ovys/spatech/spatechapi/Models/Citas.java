/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ovys.spatech.spatechapi.Models;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
/**
 *
 * @author Cris Murillo
 */
public class Citas {
  @Id
  public ObjectId _id;
  public String date;
  public String hour;
  public String service;
  public Integer time;
  public String sucursal;
  public String Employee;
  public Integer shots;
  public String user_name;
  
  // Constructors
  public Citas() {}
  
  public Citas(ObjectId _id, String date, String hour, String service, Integer time, String sucursal, String employee, Integer shots, String user_name) {
    this._id = _id;
    this.date = date;
    this.hour = hour;
    this.service = service;
    this.time = time;
    this.sucursal = sucursal;
    this.Employee = employee;
    this.shots = shots;
    this.user_name = user_name;
  }
  
  // ObjectId needs to be converted to string
  public String get_id() { return _id.toHexString(); }
  public void set_id(ObjectId _id) { this._id = _id; }
  
  public String getDate() { return date; }
  public void setDate(String date) { this.date = date; }
  
  public String getHour() { return hour; }
  public void setHour(String hour) { this.hour = hour; }
  
  public String getService() { return service; }
  public void setService(String service) { this.service = service; }
  
  public Integer getTime() { return time; }
  public void setTime(Integer time) { this.time = time; }
  
  public String getSucursal() { return sucursal; }
  public void setSucursal(String sucursal) { this.sucursal = sucursal; }
  
  public String getEmployee() { return Employee; }
  public void setEmployee(String employee) { this.Employee = employee; }
  
  public Integer getShots() { return shots; }
  public void setShots(Integer shots) { this.shots = shots; }
  
  public String getUser_Name() { return user_name; }
  public void setUser_Name(String user_name) { this.user_name = user_name; }
}
