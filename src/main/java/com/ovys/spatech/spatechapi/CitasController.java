package com.ovys.spatech.spatechapi;

import com.ovys.spatech.spatechapi.Models.Citas;
import com.ovys.spatech.spatechapi.Repositories.CitasRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

import java.util.List;
import org.bson.types.ObjectId;
import org.springframework.http.MediaType;

@RestController
@RequestMapping("/citas")
public class CitasController {
  @Autowired
  private CitasRepository repository;
  
  @CrossOrigin(origins = "http://localhost:4200",maxAge = 3600)
  @RequestMapping(value = "/", method = RequestMethod.GET)
  public List<Citas> getAllPets() {
    return repository.findAll();
  }

  @CrossOrigin(origins = "http://localhost:4200",maxAge = 3600)
  @RequestMapping(value = "/{id}", method = RequestMethod.GET)
  public Citas getPetById(@PathVariable("id") ObjectId id) {
    return repository.findBy_id(id);
  }
    

  @CrossOrigin(origins = "http://localhost:4200",maxAge = 3600)
  @RequestMapping(value = "/", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE,produces = "application/json")
  public String createPet(@Valid @RequestBody Citas cita) {
    try {
    	cita.set_id(ObjectId.get());
    	repository.save(cita);
    	return "{\"success\":1}";
    }catch(Exception e) {
    	return e.getMessage();
    }
  }
  
  @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
  public void deletePet(@PathVariable ObjectId id) {
    repository.delete(repository.findBy_id(id));
  }
}

